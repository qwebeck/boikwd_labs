import logging
from saport.simplex.solver import UnboundeLinearProgramException
from saport.simplex.model import Model 

def run():
    model = Model("example_02_solvable")

    x1 = model.create_variable("x1")
    x2 = model.create_variable("x2")
    x3 = model.create_variable("x3")

    model.add_constraint(x1 + x2 >= -150)
    model.add_constraint(x2 + x3 >= -250)
    model.add_constraint(2*x1 + x2 + x3 *2 >= -500)

    model.maximize(8 * x1 + 5 * x2 + 2*x1)

    try:
        solution = model.solve()
    except UnboundeLinearProgramException:
        logging.info("Congratulations! You found an unfesiable solution :)")
    else:
        raise AssertionError("This problem has no solution but your algorithm hasn't figured it out!")

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(message)s')
    run()
