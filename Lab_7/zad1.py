from saport.knapsack import model
from saport.integer.model import Model

model = Model("Sense of Life")

# TODO: create variables, add constraints, set objective, etc.
dishesh = ["moules mariniers", "pate de foie gras", "beluga caivar",
           "egg Benedictine", "water-thin mint", "salmon mousse"]
xs = [model.create_variable(d) for d in dishesh]


cost_constraint = 2.15 * xs[0] + 2.75 * xs[1] + 3.35 * \
    xs[2] + 3.55 * xs[3] + 4.20 * xs[4] + 5.8*xs[5] <= 50
model.add_constraint(cost_constraint)

dishes_amount = [5, 6, 7, 5, 1, 1]
for x, amount in zip(xs, dishes_amount):
    model.add_constraint(x <= amount)

objective = 3 * xs[0] + 4 * xs[1] + 4.5 * \
    xs[2] + 4.65 * xs[3] + 8 * xs[4] + 9 * xs[5]
model.maximize(objective)

solution = model.solve()

print(model)
print(solution)
