# Lab 11 - Critical Path Method

The goal of this is lab is to implement a critical path algorithm.
Additionally, one has also to build two simplex models finding the critical path duration.

TODO:
* fill missing code in: 
  * `saport.critical_path.solvers.simplex_solver_min.py`
  * `saport.critical_path.solvers.simplex_solver_max.py`
  * `saport.critical_path.solvers.cpm_solver.py`
* run `test.py` to test your code on all available instances

## SAPORT

SAPORT = Student's Attempt to Produce an Operation Research Toolkit

Includes:

* two-step simplex
* knapsack
* integer
* min-max (2-players zero-sum games)
* max-flow
* assignment problem

To run functional tests from the `tests` subdirectory, run commands with a correct `PYTHONPATH`, e.g.

`PYTHONPATH=. python tests/simplex/test.py`
