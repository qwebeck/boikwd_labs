import numpy as np
from .model import AssignmentProblem, Assignment, NormalizedAssignmentProblem
from ..simplex.model import Model
from ..simplex.expressions.expression import Expression
from dataclasses import dataclass
from typing import List 



class Solver:
    '''
    A simplex solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        model = Model("assignment")
        variables_per_worker = [
            [
                model.create_variable(f"x_{w}_{t}")
                for t in range(self.problem.size())
            ]
            for w in range(self.problem.size())
        ] 

        for worker_variables in variables_per_worker:
            for var in worker_variables:
                model.add_constraint(var <= 1)
            model.add_constraint(Expression.from_vectors(worker_variables, [1.0 for _ in worker_variables]) == 1)

        for task in range(0, self.problem.size()):
            task_variables = [wv[task] for wv in variables_per_worker]
            model.add_constraint(Expression.from_vectors(task_variables, [1.0 for _ in task_variables]) == 1)
        
        obj_variables = [x for worker_variables in variables_per_worker for x in worker_variables]
        obj_coeffs = list(self.problem.costs.reshape((1,-1))[0])
        obj_expr = Expression.from_vectors(obj_variables, obj_coeffs)

        model.minimize(obj_expr)

        solution = model.solve()

        assigned_tasks = [-1] * self.problem.original_problem.n_workers()
        for w in range(self.problem.original_problem.n_workers()):
            for t in range(self.problem.original_problem.n_tasks()):
                if solution.value(variables_per_worker[w][t]) > 0:
                    assigned_tasks[w] = t

        org_objective = sum([self.problem.original_problem.costs[w,t] for w,t in enumerate(assigned_tasks) if t >= 0])
        return Assignment(assigned_tasks, org_objective)



