import numpy as np
from .model import Assignment, AssignmentProblem, NormalizedAssignmentProblem
from typing import List, Dict, Tuple, Set

class Solver:
    '''
    A hungarian solver for the assignment problem.

    Methods:
    --------
    __init__(problem: AssignmentProblem):
        creates a solver instance for a specific problem
    solve() -> Assignment:
        solves the given assignment problem
    extract_mins(costs: np.Array):
        substracts from columns and rows in the matrix to create 0s in the matrix
    find_max_assignment(costs: np.Array) -> Dict[int,int]:
        finds the biggest possible assinments given 0s in the cost matrix
        result is a dictionary, where index is a worker index, value is the task index
    add_zero_by_crossing_out(costs: np.Array, partial_assignment: Dict[int,int])
        creates another zero(s) in the cost matrix by crossing out lines (rows/cols) with zeros in the cost matrix,
        then substracting/adding the smallest not crossed out value
    create_assignment(raw_assignment: Dict[int, int]) -> Assignment:
        creates an assignment instance based on the given dictionary assignment
    '''
    def __init__(self, problem: AssignmentProblem):
        self.problem = NormalizedAssignmentProblem.from_problem(problem)

    def solve(self) -> Assignment:
        costs = np.array(self.problem.costs)

        while True:
            self.extracts_mins(costs)
            max_assignment = self.find_max_assignment(costs)
            if len(max_assignment) == self.problem.size():
                return self.create_assignment(max_assignment)
            self.add_zero_by_crossing_out(costs, max_assignment)

    def extracts_mins(self, costs):
        self.extract_mins_from_rows(costs)
        self.extract_mins_from_cols(costs) 
    
    def extract_mins_from_rows(self, costs):
        for row in costs:
            row -= min(row)

    def extract_mins_from_cols(self, costs):
        for col in costs.T:
            col -= min(col)

    def add_zero_by_crossing_out(self, costs: np.array, partial_assignment: Dict[int,int]):
        marked_rows, crossed_cols = self.mark_rows_cols(costs, partial_assignment)
        crossed_rows = self.cross_out_rows(marked_rows)
        min_uncrossed = min([costs[r,c] for r,c in np.ndindex(costs.shape) if r not in crossed_rows and c not in crossed_cols])
        costs -= min_uncrossed
        for r in crossed_rows:
            costs[r,:] += min_uncrossed
        for c in crossed_cols:
            costs[:,c] += min_uncrossed

    def mark_rows_cols(self, costs: np.array, partial_assignment: Dict[int,int]) -> Tuple[Set[int], Set[int]]:
        size = costs.shape[0]
        marked_rows = { i for i in range(size) if i not in partial_assignment.keys() }
        marked_cols = set()

        while True:
            cols_to_mark = { c for c in range(size) if 
                c not in marked_cols and
                len([costs[r,c] for r in marked_rows if costs[r,c] == 0 ]) > 0 }
            if len(cols_to_mark) == 0:
                return marked_rows, marked_cols

            marked_cols = marked_cols.union(cols_to_mark)
            
            rows_to_mark = { r for r in range(size) if
                r not in marked_rows and 
                len([c for c in marked_cols if partial_assignment[r] == c])
            }
            marked_rows = marked_rows.union(rows_to_mark)


    def cross_out_rows(self, marked_rows: Set[int]) -> Set[int]:
        return {r for r in range(self.problem.size()) if r not in marked_rows}

    def find_max_assignment(self, costs) -> Dict[int,int]:
        partial_assignment = dict()
        zeros_per_row = { i : list(np.where(row == 0)[0]) for i,row in enumerate(costs) if 0 in row}
        zeros_per_col = { i : list(np.where(col == 0)[0]) for i,col in enumerate(costs.T) if 0 in col }

        while len(zeros_per_row) > 0:          
            row_with_min_zeros = min(zeros_per_row, key = lambda x: len(zeros_per_row.get(x)))
            cols_with_the_row = { i : col for i,col in zeros_per_col.items() if row_with_min_zeros in col}
            col_with_min_zeros = min(cols_with_the_row, key = lambda x: len(cols_with_the_row.get(x)))

            partial_assignment[row_with_min_zeros] = col_with_min_zeros
            
            zeros_per_row.pop(row_with_min_zeros)
            for row in zeros_per_row.values():
                try:
                    row.remove(col_with_min_zeros)
                except:
                    pass
            zeros_per_row = { i : row for i,row in zeros_per_row.items() if len(row) > 0 }

            zeros_per_col.pop(col_with_min_zeros)
            for col in zeros_per_col.values():
                try:
                    col.remove(row_with_min_zeros)
                except:
                    pass
            zeros_per_col = { i : col for i,col in zeros_per_col.items() if len(col) > 0 }

        return partial_assignment

    def create_assignment(self, raw_assignment: Dict[int,int]) -> Assignment:
        assignment = [-1] * self.problem.original_problem.n_workers()
        for w in range(self.problem.original_problem.n_workers()):
            t = raw_assignment[w]
            if t < self.problem.original_problem.n_tasks():
                assignment[w] = t

        total_cost = sum([self.problem.original_problem.costs[w, t] for w, t in enumerate(assignment) if t >= 0])
        return Assignment(assignment, total_cost)