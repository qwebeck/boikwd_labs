from __future__ import annotations
import ahp
from dataclasses import dataclass
import numpy as np
from typing import Callable, Tuple, Type


@dataclass
class RankingSolution:
    rankings: Tuple[np.Array]
    preference_ranking: np.Array
    global_ranking: np.Array
    choice: int


@dataclass
class ConsistencySolution:
    saaty: float
    koczkodaj: float


def ranking_assignment():
    """
    Zadanie 1. z pdf-a
    """
    Cs = (
        ahp.read_comparison_matrix("1/7,1/5;3"),
        ahp.read_comparison_matrix("5,9;4"),
        ahp.read_comparison_matrix("4,1/5;1/9"),
        ahp.read_comparison_matrix("9,4;1/4"),
        ahp.read_comparison_matrix("1,1;1"),
        ahp.read_comparison_matrix("6,4;1/3"),
        ahp.read_comparison_matrix("9,6;1/3"),
        ahp.read_comparison_matrix("1/2,1/2;1")
    )
    c_p = ahp.read_comparison_matrix(
        "4,7,5,8,6,6,2;5,3,7,6,6,1/3;1/3,5,3,3,1/5;6,3,4,1/2;1/3,1/4,1/7;1/2,1/5;1/5")

    evm_solution = rate(ahp.evm, Cs, c_p)
    gmm_solution = rate(ahp.gmm, Cs, c_p)
    return evm_solution, gmm_solution


def consistency_assignment():
    Cs = (
        ahp.read_comparison_matrix("7,3;2"),
        ahp.read_comparison_matrix("1/5,7,1;1/2,2;3"),
        ahp.read_comparison_matrix("2,5,1,7;3,1/2,5;1/5,2;7")
    )
    saaty_indexes = [ahp.saaty_index(matrix) for matrix in Cs]
    koczkodaj_indexes = [ahp.koczkodaj_index(matrix) for matrix in Cs]
    return (ConsistencySolution(s, k) for s, k in zip(saaty_indexes, koczkodaj_indexes))


def rate(ranking_method: Callable[[np.array], np.array], pairwise_comparisons: np.array,  preferences: np.array):
    """
    Wybiera najlepszą opcję z dostępnych porównań na podstawie przypisanego kosztu

    :param ranking_method: metoda rankingowa.  evm, gmm
    :param pairwise_comparisons: lista zawierająca macierze porównań każdy z każdym dla każdej opcji według każdego kryterium. 
    :param preferences: macierz zawierająca nasze eksperckie preferencje na podstawie których porównujemy kryteria.
    Trochę dokładniej z tym o co chodzi. 
    Generalnie mamy do czynienia z takim problemem: 
        U nas jest trzy domy i my chcemy ocenić który z tych domów jest lepszy.
        Żeby długo się nad tym nie zastanwiać i nie wymyślać niewiadomo co, podchodzimy do tego jako mądrze ludzie:
        siadamy i tworzymy macierz preferencji w której pokazujemy na ile jeden kryterium jest ważniejszy od innego.
        Coś w stylu:
                        cena     |        odległość 
                                 |       od centrum
        cena             1       |         5
        --------------------------------------------------
        odległość     1/5        |         1
        od centrum               |

        I to oznacza, że odległośc od centrum jest dla nas o wiele ważniejsza niż cena
        Tak powstaje parameter `preferences`. 


        Kolejna rzecz, którą robimy, to bierzermy wszystkie domy które my mamy i porównujemy je wzgłędem każdego z kryteriów.
        Czyli, najpeirw porównujemy cenę pierwszego domu z pierwszym, potem drugiego z pierwszym i tak dla każdego z kryteriów 
        powstaje coś w stylu:
                        CENA
                    dom    1     |        dom 2 
                                 |       
        dom 1            1       |         1/2
        --------------------------------------------------
        dom 2         2          |         1

        I to oznacza, że naszym zdaniem dom 1 ma lepszą cenę niż dom 2.
        Tak powstaje pairwise_comparisons.


        Okej. 
        Teraz algorytm.
        1. Tworzymy ranking według każdego z kryteriów. (wzgłędem ceny ranking mógłby wyglądać tak: [dom 1, dom 2] )
        2. Tworzymy ranking kryteriów (np. [odległośc od centrum, cena])
        3. Tworzymy globalny ranking. Czyli dla każdego z domów, mnozymy jego wagę w lokalnym rankingu razy wagę kryterium dla 
        którego ranking był ułożony
        4. Wybieramy dom, który jest najwyżej w globalnym rankingu
    """
    local_rankings = [ranking_method(matrix)
                      for matrix in pairwise_comparisons]
    cryteria_weights = ranking_method(preferences)
    global_ranking = []
    num_choices = len(local_rankings[0])
    for option in range(num_choices):
        option_weights = np.array([local_ranking[option]
                                   for local_ranking in local_rankings])
        option_score = option_weights @ cryteria_weights
        global_ranking.append(option_score)

    choice = np.where(np.array(global_ranking) ==
                      max(global_ranking))[0][0]
    return RankingSolution(
        local_rankings, cryteria_weights, global_ranking, choice)


if __name__ == '__main__':
    evm_solution, gmm_solution = ranking_assignment()
    print(evm_solution.choice)
    print(gmm_solution.choice)
    print(list(consistency_assignment()))
